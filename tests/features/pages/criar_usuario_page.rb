class User < SitePrism::Page
    set_url 'users/new'
    element :nome_usuario, '#user_name'
    element :sobrenome, '#user_lastname'
    element :email, '#user_email'
    element :endereco, '#user_address'
    element :universidade, '#user_university'
    element :profissao, '#user_profile'
    element :genero, '#user_gender'
    element :idade, '#user_age'

    element :criar, 'input[value="Criar"]'
  
    def preencher_usuario
      nome_usuario.set 'vi'
      sobrenome.set 'ge'
      email.set 'vi@gmail.com'
      endereco.set 'rua rua '
      universidade.set 'uf'
      profissao.set 'analista'
      genero.set 'Masculino'
      idade.set '25'

      criar.click
    end
end